import appConfig from './app.config';
import databaseConfig from './database.config';
import openApiConfig from "./open-api.config";

export default [appConfig, databaseConfig, openApiConfig];