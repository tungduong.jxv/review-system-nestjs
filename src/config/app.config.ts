import { registerAs } from '@nestjs/config';

interface HttpConfig {
    host: string;
    port: number;
}

export interface AppConfig {
    name: string;
    http: HttpConfig;
    globalPrefix: string;
    env: string;
}

export default registerAs(
    'app',
    (): AppConfig => ({
        env: process.env.NODE_ENV || 'development',
        name: process.env.APP_NAME || 'nest-api',
        globalPrefix: process.env.APP_GLOBAL_PREFIX || 'api',
        http: {
            host: process.env.HOST || '0.0.0.0',
            port: parseInt(process.env.PORT) || 3000,
        },
    }),
);