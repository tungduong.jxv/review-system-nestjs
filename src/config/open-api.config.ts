import { registerAs } from '@nestjs/config';

export interface OpenApiConfig {
    title: string;
    description: string;
    version: string;
}

export default registerAs(
    'open-api',
    (): OpenApiConfig => ({
        title: 'Rating System API',
        description: 'BONO Rating System.',
        version: '1.0.0',
    }),
);