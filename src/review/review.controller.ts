import {
    Body,
    Controller,
    Delete,
    Get,
    HttpException, HttpStatus,
    Param,
    Post,
    Put,
    ValidationPipe,
} from '@nestjs/common';
import { CreateReviewDto } from './dto/create-review.dto';
import { ReplyReviewDto } from "./dto/reply-review.dto";
import { ReviewService } from './review.service';
import {ApiOkResponse, ApiOperation, ApiTags} from "@nestjs/swagger";
import {Review} from "./schemas/review.schema";

@ApiTags('Reviews')
@Controller('reviews')
export class ReviewController {
    constructor(private readonly service: ReviewService) {}

    @ApiOperation({
        description: 'Get all Reviews'
    })
    @ApiOkResponse({
        description: 'The reviews were successfully obtained.',
        type: [Review],
    })
    @Get()
    async index() {
        try {
            return await this.service.findAll();
        } catch (err) {
            throw new HttpException({
                status: HttpStatus.FORBIDDEN,
                error: 'Forbidden',
            }, HttpStatus.FORBIDDEN, {
                cause: err
            });
        }
    }

    @ApiOperation({
        description: 'Get Review by ID'
    })
    @ApiOkResponse({
        description: 'The review were successfully obtained.',
        type: Review,
    })
    @Get("/:id")
    async find(@Param("id") id: string) {
        try {
            return await this.service.findOne(id);
        } catch (err) {
            throw new HttpException({
                status: HttpStatus.NOT_FOUND,
                error: 'Review not found',
            }, HttpStatus.NOT_FOUND, {
                cause: err
            });
        }
    }

    @ApiOperation({
        description: 'Create a review'
    })
    @ApiOkResponse({
        description: 'Review created successfully.',
        type: Review,
    })
    @Post()
    async create(@Body() createTodoDto: CreateReviewDto) {
        return await this.service.create(createTodoDto);
    }

    @ApiOperation({
        description: 'Reply by seller'
    })
    @ApiOkResponse({
        description: 'Seller replied successfully.',
        type: Review,
    })
    @Put('/reply')
    async update(@Body() _id: string,@Body() updateTodoDto: ReplyReviewDto) {
        return await this.service.update(_id, updateTodoDto);
    }

    @ApiOperation({
        description: 'Delete review by ID'
    })
    @ApiOkResponse({
        description: 'Review deleted successfully.',
    })
    @Delete('delete/:id')
    async delete(@Param("id") id: string) {
        try {
            return await this.service.delete(id);
        } catch (err) {
            throw new HttpException({
                status: HttpStatus.NOT_FOUND,
                error: 'Review not found',
            }, HttpStatus.NOT_FOUND, {
                cause: err
            });
        }
    }
}