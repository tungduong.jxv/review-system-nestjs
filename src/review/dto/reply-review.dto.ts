import { BaseReviewDto } from "./base-review.dto";
import { IsString, IsNotEmpty } from "class-validator";
import { ApiProperty } from "@nestjs/swagger";

export class ReplyReviewDto extends BaseReviewDto {
    @ApiProperty({
        example: 'Thanks for your feedback!',
        description: 'Seller reply'
    })
    @IsNotEmpty()
    @IsString()
    seller_reply: string;
}