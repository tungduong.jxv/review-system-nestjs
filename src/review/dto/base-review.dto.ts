import {
    IsBoolean,
    IsNotEmpty,
    IsOptional,
    IsString,
    MaxLength,
    IsObject,
    IsArray
} from 'class-validator';

export class BaseReviewDto {
    @IsNotEmpty()
    product_id: string;

    @IsNotEmpty()
    @IsString()
    @MaxLength(100)
    review_type: string;

    @IsNotEmpty()
    @IsString()
    @MaxLength(1000)
    content: string;

    @IsNotEmpty()
    @IsObject()
    ratings : {
        seller_rating: string,
        overall_rating: string,
        logistics_rating: string,
        product_rating: string
    };

    @IsNotEmpty()
    @IsBoolean()
    can_be_reply: boolean;

    @IsOptional()
    @IsArray()
    review_images: {
        image_url: string,
        image_cover_url: string,
    } [];

    @IsOptional()
    @IsArray()
    review_videos: {
        video_url: string,
        video_cover_url: string,
    } [];

    @IsOptional()
    @IsString()
    seller_reply: string;
}