import { Injectable } from '@nestjs/common';
import { InjectModel } from "@nestjs/mongoose";
import { Review, ReviewDocument } from "./schemas/review.schema";
import { CreateReviewDto } from "./dto/create-review.dto";
import {ReplyReviewDto} from "./dto/reply-review.dto";
import { Model } from "mongoose";

@Injectable()
export class ReviewService {
    constructor(
        @InjectModel(Review.name) private readonly model: Model<ReviewDocument>
    ) {}

    async findAll(): Promise<Review[]> {
        return await this.model.find().exec();
    }

    async findOne(id: string): Promise<Review> {
        return await this.model.findById(id).exec();
    }

    async create(createReviewDto: CreateReviewDto): Promise<Review> {
        return await new this.model({
            ...createReviewDto,
            createdAt: new Date(),
        }).save();
    }

    async update(id: string, replyReviewDto: ReplyReviewDto): Promise<Review> {
        return await this.model.findByIdAndUpdate(id, replyReviewDto).exec();
    }
    async delete(id: string): Promise<Review> {
        return await this.model.findByIdAndDelete(id).exec();
    }
}
