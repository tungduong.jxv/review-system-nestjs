import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document } from 'mongoose';
import { ApiProperty } from "@nestjs/swagger";

export type ReviewDocument = Review & Document;



@Schema({timestamps: true})
export class Review {
    @ApiProperty({
        example: '123456',
        description: 'Product ID'
    })
    @Prop({ required: true })
    product_id: string;

    @ApiProperty({
        example: 'PRODUCT_REVIEW',
        description: 'Review type'
    })
    @Prop({
        type: String,
        required: true,
        default: 'PRODUCT_REVIEW',
        enum: ['PRODUCT_REVIEW', 'FOLLOW_UP_REVIEW']
    })
    review_type: string;

    @ApiProperty({
        example: 'This product is good',
        description: 'Review content'
    })
    @Prop()
    content: string;

    @ApiProperty({
        example: '2022-07-19T13:01:16.532+00:00',
        description: 'Create time'
    })
    @Prop({ required: true })
    create_time: Date;

    @ApiProperty({
        example: '2022-07-19T13:01:16.532+00:00',
        description: 'Submit time'
    })
    @Prop({ required: true })
    submit_time: Date;

    @ApiProperty({
        example: 'ratings: {\n' +
            '        seller_rating: "5",\n' +
            '        overall_rating: "4",\n' +
            '        logistics_rating: "1",\n' +
            '        product_rating: "2"\n' +
            '    }',
        description: 'Ratings for each category'
    })
    @Prop({
        type: {
            seller_rating: String,
            overall_rating: String,
            logistics_rating: String,
            product_rating: String
        },
        default: {
            seller_rating: "1",
            overall_rating: "1",
            logistics_rating: "1",
            product_rating: "1"
        }
    })
    ratings: {
        seller_rating: string,
        overall_rating: string,
        logistics_rating: string,
        product_rating: string
    }

    @ApiProperty({
        example: 'review_images: [{\n' +
            '        image_url: "abcd.com/1.img",\n' +
            '        image_cover_url: "abcd.com/1_cover.img",\n' +
            '    }]',
        description: 'Review Images'
    })
    @Prop()
    review_images: {
        image_url: string,
        image_cover_url: string,
    } [];

    @ApiProperty({
        example: 'review_videos: [{\n' +
            '        video_url: "abcd.com/1.img",\n' +
            '        video_cover_url: "abcd.com/1_cover.img",\n' +
            '    }]',
        description: 'Review videos'
    })
    @Prop()
    review_videos: {
        video_url: string,
        video_cover_url: string,
    } [];

    @ApiProperty({
        example: 'true',
        description: 'Check if review can be reply by seller'
    })
    @Prop({ required: true, default: true })
    can_be_reply: boolean;

    @ApiProperty({
        example: 'Thanks for your feedback!',
        description: 'Seller reply'
    })
    @Prop()
    seller_reply: string;
}

export const ReviewSchema = SchemaFactory.createForClass(Review);