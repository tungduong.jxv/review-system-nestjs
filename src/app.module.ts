import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { ReviewModule } from './review/review.module';

//Mongoose
import { MongooseModule } from '@nestjs/mongoose';
import {ConfigModule} from "@nestjs/config";
import { DatabaseModule } from './database/database.module';
import { DatabaseService } from "./database/database.service";
import configs from "./config/index";

@Module({
  imports: [
      ReviewModule,
      MongooseModule.forRootAsync({
          inject: [DatabaseService],
          imports: [DatabaseModule],
          useFactory: (databaseService: DatabaseService) => databaseService.createMongooseOptions(),
      }),
      ConfigModule.forRoot({
          load: configs,
          isGlobal: true,
      }),
      DatabaseModule
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
